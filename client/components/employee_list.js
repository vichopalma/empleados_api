import React, { Component } from 'react';
// old import { createContainer } from 'meteor/react-meteor-data';
import { withTracker } from 'meteor/react-meteor-data'; //new
import { Employees } from '../../imports/collections/employees';

import EmployeeDetail from './employee_detail';

const PER_PAGE = 20;

class EmployeeList extends Component {

componentWillMount(){
  this.page = 1;
}

handlerButtonClick(){
  Meteor.subscribe('employees', PER_PAGE * (this.page + 1) );
  this.page +=1;
}

render(){
  return(
  <div>
    <div className="employee-list">
      {this.props.employees.map(employee =>
        <EmployeeDetail key={employee._id} employee={employee}/>)}
    </div>
    <button onClick={this.handlerButtonClick.bind(this)}
      className="btn btn-primary">Cargar mas...</button>
  </div>
  );
 }
};

/*
//old form for a container
export default createContainer(() => {
  // set up subscription
Meteor.subscribe('employees');
  // return an object. whatever we return will be sent to EmployeeList
  // as props
return { employees: Employees.find({}).fetch()};
},EmployeeList);
*/

//container, tracker de datos
export default withTracker(() => {
  // Do all your reactive data access in this method.
  // Note that this subscription will get cleaned up when your component is unmounted
  // set up subscription
Meteor.subscribe('employees', PER_PAGE);
  // return an object. whatever we return will be sent to EmployeeList
  // as props

  return { employees: Employees.find({}).fetch() };
})(EmployeeList);
